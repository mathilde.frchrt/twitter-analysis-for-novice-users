# Twitter analysis for novice users

This tutorial allows novice users to use Jupyter notebook to analyze Twitter data

## Goal

We developed a R Notebook to enable novice users to easily and autonomously analyze data from Twitter. We tried it out using Jupyter Notebook with a class of 44 students without programming skills. They were able to adapt and execute the notebook to describe various data sets of tweets.

## Topics

- smoking, 
- anxiety, 
- paranoia, 
- schizophrenia, 
- bipolarity, 
- eating disorders, 
- sleep disorders, 
- handicap, 
- cannabis, 
- hard drugs, 
- nitrous oxide, 
- GHB,  
- supervised injection site, 
- denial of pregnancy, 
- miscarriage, 
- postpartum, 
- sexual assault, 
- vaccine pass, 
- endometriosis, 
- harassment,  
- antidepressants. <br>
We extracted 1000 tweets of each topics.

## Assessment

We assessed the ability of the students to use the Jupyter Notebook in checking if they were able to perform the following tasks:
- instructions in relation to the notebook (5 questions): modify and complete cells, execute cells, create new cells, export in pdf:  <br>
*q1. Add your name in the title "introduction to big data" by double clicking on the cell <br>
q2. Run the code area below <br>
q3. Click on this cell, then on the + button (top left) to insert a new cell after it. By default, this cell runs code. Place a very simple addition and execute the cell.<br>
q4. Click on this cell, then on the + button (top left) to insert a new cell after it. By default, this cell runs code. Change **Code** to **Markdown** and fill in the date.<br>
q14. Export the notebook in PDF format (top left, File / Export Notebook As... / PDF. Send it to me by email.* <br>

- R code 1st level (6 questions): from an existing and positioning code, compute a new variable, change a color in a graphic, change a variable in a graphic: <br>
*q5. Modify the following statement to select the **RetweetCount** and "ReplyCount" columns.<br>
q6. Modify the following instruction to filter the tweets and keep only those for which the number of likes is higher than 20.<br>
q7. Modify the statement by replacing the **lubridate::wday** function with the **lubridate::month** function to extract the month.<br>
q8. Modify the statement to group by time (column **hour**) and count the number of tweets<br>
q12. Adapt the code below so that the color of the word cloud ("COLOR") is "RdBu".<br>
q13. Adapt the code below to analyze and graph the most frequent mentions (@) by replacing the # pattern with @.<br>*

- R code 2st level (3 questions): find a instruction already implemented and modify it to produce a new result: <br>
*q9. Take the code below and adapt it to produce the graph of the number of tweets per hour, from the table **tweets_by_hour**. Change the title of the graph.<br>
q10. Modify the following code to produce the like count histogram. Change the title of the graph and the title of the y-axis)<br>
q11. Adapt the code below so that the color of the graph is "red".<br>*

We also asked them to give us feedback on the difficulties encountered, the positive points, and ideas for improvement.

## Results

The average (standard-deviation) rate of correct answers was 90.7 (13.3) for all the questions, and 92.3 (7.1), 94,7 (10,8) and 79,9 (22,9) for instructions in relation to the notebook, R code 1st level and R code 2nd level, respectively. The figure 1 represents the rate of correct answer per question.

![result_assessment.png](./result_assessment.png)


